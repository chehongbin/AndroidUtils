package com.oldfeel.base;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oldfeel.utils.ETUtil;
import com.oldfeel.utils.R;
import com.oldfeel.utils.ViewUtil;

/**
 * fragment基类
 *
 * @author oldfeel
 *         <p/>
 *         Created on: 2014-1-20
 */
public class BaseFragment extends Fragment {

    protected DisplayImageOptions options;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    protected int defaultImageId = R.drawable.ic_launcher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (defaultImageId > 0) {
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(defaultImageId).showImageOnFail(defaultImageId)
                    .cacheInMemory(true).cacheOnDisc(true).build();
        }
    }

    protected void showToast(String text) {
        ((BaseActivity) getActivity()).showToast(text);
    }

    protected void showSimpleDialog(String text) {
        ((BaseActivity) getActivity()).showSimpleDialog(text);
    }

    protected void openActivity(Class<?> className) {
        Intent intent = new Intent(getActivity(), className);
        startActivity(intent);
    }

    public String getString(EditText et) {
        return ETUtil.getString(et);
    }

    public GridView getGridView(View view, int id) {
        return ViewUtil.getGridView(view, id);
    }

    public ListView findListView(View view, int id) {
        return ViewUtil.getListView(view, id);
    }

    public TextView getTextView(View view, int id) {
        return ViewUtil.getTextView(view, id);
    }

    public ImageView getImageView(View view, int id) {
        return ViewUtil.getImageView(view, id);
    }

    public ListView getListView(View view, int id) {
        return ViewUtil.getListView(view, id);
    }

    public Button getButton(View view, int id) {
        return ViewUtil.getButton(view, id);
    }

    public EditText getEditText(View view, int id) {
        return ViewUtil.getEditText(view, id);
    }

    public LinearLayout getLinearLayout(View view, int id) {
        return ViewUtil.getLinearLayout(view, id);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right,
                R.anim.slide_out_left);
    }
}
