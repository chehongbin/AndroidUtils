package com.oldfeel.base;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class BasePagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> list = new ArrayList<Fragment>();

    public BasePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void add(Fragment fragment) {
        list.add(fragment);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Fragment fragment = list.get(position);
        String title = fragment.getArguments().getString("title");
        if (title != null) {
            return title;
        }
        return super.getPageTitle(position);
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}