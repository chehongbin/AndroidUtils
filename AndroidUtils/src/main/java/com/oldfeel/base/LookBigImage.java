package com.oldfeel.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oldfeel.utils.R;

/**
 * Created by oldfeel on 4/2/15.
 */
public class LookBigImage extends BaseActivity {
    TextView tvIndicator;
    ViewPager pager;
    BasePagerAdapter adapter;
    String[] images;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.look_big_image);
        pager = (ViewPager) findViewById(R.id.look_big_image_pager);
        tvIndicator = (TextView) findViewById(R.id.look_big_image_indicator);
        adapter = new BasePagerAdapter(getFragmentManager());
        pager.setAdapter(adapter);
        images = getIntent().getStringArrayExtra("images");
        position = getIntent().getIntExtra("position", 0);
        for (int i = 0; i < images.length; i++) {
            adapter.add(BigImageFragment.newInstance(images[i]));
        }
        pager.setCurrentItem(position);
        tvIndicator.setText((position + 1) + "/" + images.length);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tvIndicator.setText((position + 1) + "/" + images.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public static class BigImageFragment extends BaseFragment {
        private String imagePath;
        private ImageView imageView;

        public static BigImageFragment newInstance(String imagePath) {
            BigImageFragment fragment = new BigImageFragment();
            fragment.imagePath = imagePath;
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.item_look_big_image, container, false);
            imageView = (ImageView) view.findViewById(R.id.item_look_big_image);
            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            imageLoader.displayImage(imagePath, imageView, options);
        }
    }
}
