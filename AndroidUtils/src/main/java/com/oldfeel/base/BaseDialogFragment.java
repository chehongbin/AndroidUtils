package com.oldfeel.base;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.VideoView;

import com.baidu.mobstat.StatService;
import com.oldfeel.interfaces.FragmentListener;
import com.oldfeel.utils.DialogUtil;
import com.oldfeel.utils.ETUtil;
import com.oldfeel.utils.R;
import com.oldfeel.utils.ViewUtil;

/**
 * dialogfragment基类
 *
 * @author oldfeel
 *         <p/>
 *         Created on: 2014-1-14
 */
public abstract class BaseDialogFragment extends DialogFragment {
    private boolean isShowDelete = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_dialog_fragment, container, false);
        FrameLayout flContent = (FrameLayout) view.findViewById(R.id.base_dialog_fragment_content);
        Button btnOk = (Button) view.findViewById(R.id.base_dialog_fragment_ok);
        Button btnDelete = (Button) view.findViewById(R.id.base_dialog_fragment_delete);
        Button btnCancle = (Button) view.findViewById(R.id.base_dialog_fragment_cancel);
        if (isShowDelete) {
            btnDelete.setVisibility(View.VISIBLE);
        }
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOkClicked();
                dismiss();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteClicked();
                dismiss();
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClicked();
                dismiss();
            }
        });
        flContent.addView(onCreateContentView(inflater, container, savedInstanceState));
        return view;
    }

    public abstract View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public FragmentListener fragmentListener;

    public void setOnFragmentListener(FragmentListener fragmentListener) {
        this.fragmentListener = fragmentListener;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (fragmentListener != null) {
            fragmentListener.onCreated();
        }
    }

    public void onOkClicked() {
        if (fragmentListener != null) {
            fragmentListener.onComplete();
        }
    }

    public void onDeleteClicked() {

    }

    public void onCancelClicked() {
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        onCancelClicked();
    }

    public VideoView getVideoView(View view, int id) {
        return ViewUtil.getVideoView(view, id);
    }

    public ListView getListView(View view, int id) {
        return ViewUtil.getListView(view, id);
    }

    public EditText getEditText(View view, int id) {
        return ViewUtil.getEditText(view, id);
    }

    public ImageView getImageView(View view, int id) {
        return ViewUtil.getImageView(view, id);
    }

    public GridView getGridView(View view, int id) {
        return ViewUtil.getGridView(view, id);
    }

    public Button getButton(View view, int id) {
        return ViewUtil.getButton(view, id);
    }

    public ImageButton getImageButton(View view, int id) {
        return ViewUtil.getImageButton(view, id);
    }

    public void showToast(String text) {
        DialogUtil.getInstance().showToast(getActivity(), text);
    }

    public String getString(EditText et) {
        return ETUtil.getString(et);
    }

    @Override
    public void onPause() {
        StatService.onPause(this);
        super.onPause();
    }

    @Override
    public void onResume() {
        StatService.onResume(this);
        super.onResume();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right,
                R.anim.slide_out_left);
    }

    public void setShowDelete(boolean isShowDelete) {
        this.isShowDelete = isShowDelete;
    }
}
