package com.oldfeel.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * json工具类
 *
 * @author oldfeel
 *         Created on: 2014年2月17日
 */
public class JsonUtil {

    /**
     * @return true 为成功,false为失败
     */
    public static boolean isSuccess(String result) {
        return getCode(result) == 0;
    }

    public static String getData(String result) {
        try {
            JSONObject json = new JSONObject(result);
            return json.get("Data").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getCode(String result) {
        try {
            JSONObject json = new JSONObject(result);
            return json.getInt("Code");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
