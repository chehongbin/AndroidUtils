package com.oldfeel.interfaces;

/**
 * 确定按钮监听
 *
 * @author oldfeel
 *         <p/>
 *         Created on: 2014-1-15
 */
public interface FragmentListener {
    public void onCreated();

    public void onComplete(Object... objects);

    public void onDelete(Object... objects);
}